# mcmcInterface change log
* 2023-07-27, Diagnosed timing bug in osInterfaces::spawnBashCommand
    * Additional bug details and code changes are in os-agnostic-utils
    * The above manifested itself when we had a quickly finishing sampler command, the call to the sampler would never return (it would hang)
    * With the fix, spawned commands now need to have a fixed 5second startup delay that allows for the pid to be properly located in the forked process running the actual bash command.  
        * Example commands are in Stan.m where we create bash scripts to build the project file and to run a sampling session
    * The update was tested on both Windows and Redhat.
