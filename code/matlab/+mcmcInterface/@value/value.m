classdef value
    %VALUE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        hValueAttributes %handle to the corresponding valueAttributes object
        type %mcmcInterface.t_valueDataType
        tfPre %mcmcInterface.t_valueTransformPre
        tfPost %mcmcInterface.t_valueTransformPost
        data %array with the data

        %fields to save when performing value transforms
        mean
    end

    methods (Static)
        function setValue = readStructFromXMLNode(valueNode)
            %stdout=1;
            %fprintf(stdout, '\tLoading the value from valueNode\n');

            tempStr=strcat('mcmcInterface.t_valueDataType.',cast(valueNode.getAttribute('type'),'char'));
            if strcmp(tempStr,'mcmcInterface.t_valueTransformPre.')==true            
                error('type missing from value node');
            else
                setValue.type=mcmcInterface.t_valueDataType.string2enum(tempStr);                
            end

            tempStr=strcat('mcmcInterface.t_valueTransformPre.',cast(valueNode.getAttribute('tfPre'),'char'));
            if strcmp(tempStr,'mcmcInterface.t_valueTransformPre.')==true
                setValue.tfPre=mcmcInterface.t_valueTransformPre.none;%none if tfPre missing
            else
                setValue.tfPre=mcmcInterface.t_valueTransformPre.string2enum(tempStr);                            
            end            

            tempStr=strcat('mcmcInterface.t_valueTransformPost.',cast(valueNode.getAttribute('tfPost'),'char'));
            if strcmp(tempStr,'mcmcInterface.t_valueTransformPost.')==true
                setValue.tfPost=mcmcInterface.t_valueTransformPost.none;%none if tfPost missing
            else
                setValue.tfPost=mcmcInterface.t_valueTransformPost.string2enum(tempStr);                 
            end   

            if (strcmp("value",valueNode.getNodeName())==1)
                setValue.data=cast(valueNode.getTextContent(),'char');
            else
                error('unrecognized node');
            end
        end

        function retNum = fromString(strNum, dimensions, type)
        %FROMSTRING
        %  Converts uses specified dimensions to convert input string into an array of numbers of type form

            % 1-strip all whitespace and newlines
            % 2-run fromStringChecks

            if ~ischar(strNum)
                error('Input to value::fromString is not a string');
            end

            if isempty(strNum)
                if (strcmp(type,'int32')==true)
                    retNum=int32.empty;
                elseif (strcmp(type,'double')==true)
                    retNum=double.empty;
                elseif (strcmp(type,'categorical')==true)
                    retNum=categorical.empty;
                end
                return
            end

            cleanstring=strrep(strNum,' ','');%remove any whitespace
            temp=strrep(cleanstring,newline,'');%remove any newlines
            cleanstring=strrep(temp,sprintf('\r'),'');%remove any carriage returns
            mcmcInterface.value.fromStringChecks(cleanstring);%check cleanstring for illegal characters

            openBracketPos=strfind(cleanstring,'[');%create an array with positions for all '['
            closeBracketPos=strfind(cleanstring,']');%create an array with positions for all ']'
            numOpenBrackets=size(openBracketPos,1);
            numCloseBrackets=size(closeBracketPos,1);

            if dimensions==mcmcInterface.t_valuesDimensions.matrix2D
                if size(openBracketPos,1)<2
                    error('matrix2D requires outer pair of []');
                end
                % strip the outer pair of brackets
                openBracketPos=openBracketPos(2:end);
                closeBracketPos=closeBracketPos(2:end);

                %check that we have at least one page
                if numel(openBracketPos)>1
                    %iterate over the pages defined by [] pairs in [[];[];[];[]]
                    for page=1:numel(openBracketPos)
                        pagestartpos=openBracketPos(page);%current opening bracket
                        pageendpos=closeBracketPos(page);%current closing bracket
                        pagestring=cleanstring(pagestartpos+1:pageendpos-1);
                        retNum=mcmcInterface.value.stringToMatrix2D(pagestring, type);
                    end         
                else
                    error('value of mcmcInterface.t_valuesDimensions.matrix2D need at least one page (i.e., single matrix) specified')
                end
            else
                if dimensions==mcmcInterface.t_valuesDimensions.scalar
                    valstring=cleanstring(1:end);
                    retNum=mcmcInterface.value.stringToScalar(valstring, type);
                elseif dimensions==mcmcInterface.t_valuesDimensions.vector
                    %should only be one pair of []
                    if ~((numOpenBrackets==1) && (numCloseBrackets==1))
                        error('value for mcmcInterface.t_valuesDimensions.vector type should have one pair of [] characters');
                    end                    
                    startpos=openBracketPos(1);%current opening bracket
                    endpos=closeBracketPos(1);%current closing bracket
                    if endpos~=length(cleanstring)
                        error('detected a malformed value string with unexpected characters after closing ]')
                    end
                    valstring=cleanstring(startpos:endpos);
                    retNum=mcmcInterface.value.stringToVector(valstring, type);
                else
                    error('value should be either mcmcInterface.t_valuesDimensions.scalar or mcmcInterface.t_valuesDimensions.vector')
                end
            end
        end

        function fromStringChecks(setstring)
            % 1-only characters allowed are [];,0123456789eE+-
            for charidx=1:size(setstring,1)
                switch (setstring(charidx))
                    case {'[',']',';',',','0','1','2','3','4','5','6','7','8','9','.','e','+','-','E'}
                        continue
                    otherwise
                        error('illegal character found in setstring');
                end
            end

            if (~(contains(setstring,'[')) && ~(contains(setstring,']')))
                return %no [] in setstring
            end

            % 2-if there are any [], total number of [ characters should match number of ]
            % 3-if there are any [], first character shoult be [ and last character should be ]
            openBracketPos=strfind(setstring,'[');%create an array with positions for all '['
            closeBracketPos=strfind(setstring,']');%create an array with positions for all ']'

            numOpenBrackets=size(openBracketPos,1);
            numCloseBrackets=size(closeBracketPos,1);
            if numOpenBrackets~=numCloseBrackets
                %check 2
                error('setstring does not have a matching number of brackets');
            end
            if setstring(1)~='[' || setstring(end)~=']'
                %check 3
                error('setstring either does not start with [ or end with ]');
            end
        end

        function retNum=stringToScalar(strNum, type)
            %either single number
            %  e.g., 1.0
            %or array of numbers in [] delimited by ;
            %  e.g., [1.0;2.0;3.0]

            %error checks for more than one []
            openBracketPos=strfind(strNum,'[');%create an array with positions for all '['
            closeBracketPos=strfind(strNum,']');%create an array with positions for all ']'
            numOpenBrackets=size(openBracketPos,1);
            numCloseBrackets=size(closeBracketPos,1);
            if ((numOpenBrackets>1) || (numCloseBrackets>1))
                error('invalid scalar in string');
            end
            %error check for any commas
            if (contains(strNum,','))
                error('scalar should not have commas');
            end

            retNum=cast(str2num(strNum),type); 
        end        
      
        function retNum=stringToVector(strNum, type)
            %array of numbers in [] with rows delimited by ; and columns delimited by ,
            %  e.g., [55707,Inf; 255092,Inf; 56776,Inf; 111646,Inf; 11358772,Inf; 875209,Inf; 68978,Inf]

            %error checks as must have only one []
            openBracketPos=strfind(strNum,'[');%create an array with positions for all '['
            closeBracketPos=strfind(strNum,']');%create an array with positions for all ']'
            numOpenBrackets=size(openBracketPos,1);
            numCloseBrackets=size(closeBracketPos,1);
            if ~((numOpenBrackets==1) && (numCloseBrackets==1))
                error('invalid vector in string');
            end

            retNum=cast(str2num(strNum),type); 
        end

        function retNum=stringToMatrix2D(strNum, type)
            %check if we are an array of vector (inside single pair of brackets, columns delimited by commas, rows delimited by ;) or real Matrix2D (outer pair of brackets, inner pages surrounded by brackets and delimited by ;)
            
            %error checks as must have > one []
            openBracketPos=strfind(strNum,'[');%create an array with positions for all '['
            closeBracketPos=strfind(strNum,']');%create an array with positions for all ']'
            numOpenBrackets=size(openBracketPos,1);
            numCloseBrackets=size(closeBracketPos,1);
            if ~((numOpenBrackets>1) && (numCloseBrackets>1))
                error('invalid vector in string');
            end

            %strip the outer brackets
            strInner=strNum(openBracketPos(2):closeBracketPos(end-1));
            openBracketPos=strfind(strInner,'[');%create an array with positions for all '['
            closeBracketPos=strfind(strInner,']');%create an array with positions for all ']'
            
            numPages=size(openBracketPos,1);
            
            for (page=1:numPages)
                pagestr=strInner(openBracketPos(page):closeBracketPos(page));
                retNum(:,:,page)=cast(str2num(pagestr),type);
            end
        end
        
        function retString=toString_alt(value, dimensions)
            switch dimensions
                case mcmcInterface.t_valuesDimensions.scalar
                    retString=mcmcInterface.value.ScalarToString(value);
                case mcmcInterface.t_valuesDimensions.vector
                    retString=mcmcInterface.value.VectorToString(value);
                case mcmcInterface.t_valuesDimensions.matrix2D
                    retString=mcmcInterface.value.Matrix2DToString(value);
                otherwise
                    error('Unsupported t_valuesDimensions for value');
            end            
        end            

        function retString=ScalarToString(value)
            %we can infer type from isa(value,'double') or isa(value,'int32')
            %check if we are an array of scalar
            retString='';
            [rows,cols]=size(value);
            if (cols>1)
                error('There should not be any columns in a scalar or array of scalar');
            end
            if (rows>1)%it is an array of scalar
                col=1;
                for row=1:rows
                    if row==1
                        retString='[';%add the opening '['               
                    end
                    if ~(row==rows)
                        if isa(value(row,col),'int8') || isa(value(row,col),'int16') || isa(value(row,col),'int32') || isa(value(row,col),'int64') || ...
                           isa(value(row,col),'uint8') || isa(value(row,col),'uint16') || isa(value(row,col),'uint32') || isa(value(row,col),'uint64')
                            retString=sprintf('%s%d;',retString,value(row,col));%add a semicolon
                        else
                            retString=sprintf('%s%f;',retString,value(row,col));%add a semicolon
                        end
                    else
                        if isa(value(row,col),'int8') || isa(value(row,col),'int16') || isa(value(row,col),'int32') || isa(value(row,col),'int64') || ...
                           isa(value(row,col),'uint8') || isa(value(row,col),'uint16') || isa(value(row,col),'uint32') || isa(value(row,col),'uint64')
                            retString=sprintf('%s%d]',retString,value(row,col));%no semicolon or comma on last row and add the closing ']'
                        else
                            retString=sprintf('%s%f]',retString,value(row,col));%no semicolon or comma on last row and add the closing ']'
                        end
                    end
                end                
            else %not an array
                if isa(value,'double')
                    retString=sprintf('%.6g',value);%minimum 6 significant digits
                elseif isa(value,'int8') || isa(value,'int16') || isa(value,'int32') || isa(value,'int64') || ...
                        isa(value,'uint8') || isa(value,'uint16') || isa(value,'uint32') || isa(value,'uint64')
                    retString=sprintf('%d',value);
                else
                    error('ScalarToString() unsupported scalar type');
                end
            end
        end

        function retString=VectorToString(value)
            %we can infer type from isa(value,'double') or isa(value,'int32')
            retString='';
            [rows,cols]=size(value);
            for row=1:rows
                if row==1
                    retString='[';%add the opening '['              
                end
                for col=1:cols
                    if ~(col==cols)
                        if isa(value(row,col),'int8') || isa(value(row,col),'int16') || isa(value(row,col),'int32') || isa(value(row,col),'int64') || ...
                           isa(value(row,col),'uint8') || isa(value(row,col),'uint16') || isa(value(row,col),'uint32') || isa(value(row,col),'uint64')
                            retString=sprintf('%s%d,',retString,value(row,col));%add a comma
                        else
                            retString=sprintf('%s%f,',retString,value(row,col));%add a comma
                        end
                    else
                        if isa(value(row,col),'int8') || isa(value(row,col),'int16') || isa(value(row,col),'int32') || isa(value(row,col),'int64') || ...
                           isa(value(row,col),'uint8') || isa(value(row,col),'uint16') || isa(value(row,col),'uint32') || isa(value(row,col),'uint64')
                            retString=sprintf('%s%d',retString,value(row,col));%no comma on last col
                        else
                            retString=sprintf('%s%f',retString,value(row,col));%no comma on last col
                        end
                    end
                end
                if (row==rows)
                    retString=sprintf('%s]',retString);%no semicolon and add the closing ']'
                else
                    retString=sprintf('%s;',retString);%add a semicolon
                end
            end
        end

        function retString=Matrix2DToString(value)
            %we can infer type from isa(value,'double') or isa(value,'int32')
            retString='';
            [rows,cols,pages]=size(value);
            
            retString='[';%add the opening '[' for the 3d matrix
            for page=1:pages
                for row=1:rows
                    if row==1
                        retString=sprintf('%s[',retString);%add the opening '[' for the page              
                    end
                    for col=1:cols
                        if ~(col==cols)
                            if isa(value(row,col,page),'int8') || isa(value(row,col,page),'int16') || isa(value(row,col,page),'int32') || isa(value(row,col,page),'int64') || ...
                               isa(value(row,col,page),'uint8') || isa(value(row,col,page),'uint16') || isa(value(row,col,page),'uint32') || isa(value(row,col,page),'uint64')
                                retString=sprintf('%s%d,',retString,value(row,col,page));%add a comma
                            else
                                retString=sprintf('%s%f,',retString,value(row,col,page));%add a comma
                            end
                        else
                            if isa(value(row,col,page),'int8') || isa(value(row,col,page),'int16') || isa(value(row,col,page),'int32') || isa(value(row,col,page),'int64') || ...
                               isa(value(row,col,page),'uint8') || isa(value(row,col,page),'uint16') || isa(value(row,col,page),'uint32') || isa(value(row,col,page),'uint64')
                                retString=sprintf('%s%d',retString,value(row,col,page));%no comma on last col
                            else
                                retString=sprintf('%s%f',retString,value(row,col,page));%no comma on last col
                            end
                        end
                    end
                    if (row==rows)
                        retString=sprintf('%s]',retString);%no semicolon and add the closing ']' for the page 
                    else
                        retString=sprintf('%s;',retString);%add a semicolon
                    end
                end
            end
            retString=sprintf('%s]',retString);%add the closing ']' for the 3d matrix             
        end       

        function retValue = constructor(setHValueAttributes,setType,setTfPre,setTfPost,setData)
            %stdout=1;
    		%fprintf(stdout, 'Constructing the values object\n');
            setValue.hValueAttributes=setHValueAttributes;
            setValue.type=mcmcInterface.t_valueDataType.string2enum(strcat('mcmcInterface.t_valueDataType.',setType));
            setValue.tfPre=mcmcInterface.t_valueTransformPre.string2enum(strcat('mcmcInterface.t_valueTransformPre.',setTfPre));
            setValue.tfPost=mcmcInterface.t_valueTransformPost.string2enum(strcat('mcmcInterface.t_valueTransformPost.',setTfPost));
            setValue.data=mcmcInterface.value.fromString(setData,obj.hValueAttributes.dimensions,obj.hValueAttributes.type);
        
            retValue=mcmcInterface.value(setHValueAttributes, setValue.type, setValue.tfPre, setValue.tfPost, setValue.data);
        end         
    end
    
    methods
        function obj = value(setHValueAttributes, setType, setTfPre, setTfPost, setData)
            %VALUE Construct an instance of this class
            %   Detailed explanation goes here
            obj.hValueAttributes=setHValueAttributes;
            obj.type=setType;
            obj.tfPre=setTfPre;
            obj.tfPost=setTfPost;
            obj.data=setData;
        end

        function retString=toString(obj)
            retString='';
            switch obj.hValueAttributes.dimensions
                case mcmcInterface.t_valuesDimensions.scalar
                    retString=mcmcInterface.value.ScalarToString(obj.data);
                case mcmcInterface.t_valuesDimensions.vector
                    retString=mcmcInterface.value.VectorToString(obj.data);
                case mcmcInterface.t_valuesDimensions.matrix2D
                    retString=mcmcInterface.value.Matrix2DToString(obj.data);
                otherwise
                    error('Unsupported t_valuesDimensions for value');
            end            
        end    
    end
end

