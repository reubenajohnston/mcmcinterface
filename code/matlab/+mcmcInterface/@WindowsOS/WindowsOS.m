classdef WindowsOS < mcmcInterface.osInterfaces
    %WindowsOS Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        
    end
    
    methods
        function obj = WindowsOS(setDirTemp,setDirLogs,setFileKill,setPython,setPythonUtilsPackage,setJavac,setDirOsInterfaces,setFileOsInterfaces)
            %WindowsOS Construct an instance of this class
            %   Detailed explanation goes here
            if nargin == 0
                superargs={};
            else
                superargs{1}=mcmcInterface.t_osInterfaces.WindowsOS;
                superargs{2}=setDirTemp;
                superargs{3}=setDirLogs;
                superargs{4}=setFileKill;
                superargs{5}=setPython;
                superargs{6}=setPythonUtilsPackage;
                superargs{7}=setJavac;
                superargs{8}=setDirOsInterfaces;
                superargs{9}=setFileOsInterfaces;
            end

            obj=obj@mcmcInterface.osInterfaces(superargs{:});      
        end
    end
end

