classdef data
    %DATA
    %   This class, mcmcInterface.data, is a data structure used to contain a data set for the simulation.  
    
    properties (Constant, Access = private) %define as private so that subclasses can have their own instances with same name
        version='1.0';      % API version for this class
    end

    properties
        uuid                % unique identifier for the data set
        description         % description of the data set
        author              % creator of the data set file
        reference           % citations and useful sources
        elements            % array of element(numData) objects with the data information
        dirData             % directory that contains data *.xml file to load
        fileData            % filename of the data *.xml file to load
    end

    methods (Static)
        function setData=readStructFromXMLFile(xmlfile,myrepos)        
            %readStructFromXMLFile 
            %   Load a simulation object from *.xml.
            % xmlfile-simulation xmlfile name (either absolute or relative path inside repos's config directory)
            % myrepos (optional)-myrepos object with base dir and list of repositories
            stdout=1;
            fprintf(stdout, 'Loading %s for the data\n',xmlfile);

            doc = xmlread(xmlfile);
            %fprintf(1,'Parsed %s\n',xmlfile);
            %javaaddpath(mcmcBayes.osInterfaces.getPath(mcmcBayes.t_paths.javaDir));
            %call our java hacks method to insert the DOCTYPE element
            entityname='data';
            %     xmlStr=XmlHacks.insert(doc, entityname, dtdfile);%call Java function  (XmlHacks must be on javaclasspath)
            %     %now parse the hacked xml
            %     doc = XmlHacks.xmlreadstring(xmlStr);
            
            doc.getDocumentElement().normalize();
            rootNode=doc.getDocumentElement();
            rootname=rootNode.getNodeName();
            if ~(strcmp(rootname,entityname)==1)
                error('Root element should be entityname');
            end

            setData=mcmcInterface.data.readStructFromXMLNode(rootNode, myrepos);                         
        end

        function setData=readStructFromXMLNode(rootNode, myrepos)
            %readStructFromXMLNode
            %   Worker function that parses and returns information within a structure that represents a root element.  Function can also be called
            %with a sparse structure, that only contains some (and not all) fields in the root element (so this function must handle missing entries).
            % rootNode-data structure with the xml node read in via xmlread
            % myrepos (optional)-myrepos object with base dir and list of repositories

            stdout=1;
            fprintf(stdout, '\tLoading the data from rootNode\n');
            
            %check version attribute
            tversion=cast(rootNode.getAttribute('version'),'char');
            if (strcmp(mcmcInterface.data.getVersion(),tversion)~=1)
                error('data version does not match in *.xml');
            end

            %get the root attributes (if they exist)
            tattributes=rootNode.getAttributes();
            for i=1:tattributes.getLength()
                attributeName=cast(tattributes.item(i-1).getName(),'char');
                if (strcmp(attributeName,'uuid')==1)
                    setData.uuid=cast(tattributes.item(i-1).getTextContent(),'char');
                elseif (strcmp(attributeName,'description')==1)
                    setData.description=cast(tattributes.item(i-1).getTextContent(),'char');
                elseif (strcmp(attributeName,'author')==1)
                    setData.author=cast(tattributes.item(i-1).getTextContent(),'char');
                elseif (strcmp(attributeName,'reference')==1)
                    setData.reference=cast(tattributes.item(i-1).getTextContent(),'char');
                end
            end

            loadStatus.dirData=false;
            loadStatus.fileData=false;
            numElements=0;
            for i=1:rootNode.getLength()
                tNode=rootNode.item(i-1);
                if (strcmp(tNode.getNodeName,'dir')==1)
                    tDir=mcmcInterface.storageObjectPath(tNode,myrepos);
                    if (strcmp(tDir.name,'dirData')==1)
                        loadStatus.dirData=true;
                        setData.dirData=tDir.path;
                    else
                        error('unexpected xml node name encountered for dir');
                    end                       
                elseif (strcmp(tNode.getNodeName,'file')==1)
                    tFile=mcmcInterface.storageObjectPath(tNode,myrepos);
                    if (strcmp(tFile.name,'fileData')==1)
                        if (loadStatus.dirData==false)
                            error('need to have dirData in xml');
                        end
                        loadStatus.fileData=true;
                        setData.fileData=tFile.path;
                        tfilename=strcat(setData.dirData,mcmcInterface.osInterfaces.getNativeSeparator(),setData.fileData);
                        setDataOverload=mcmcInterface.data.readStructFromXMLFile(tfilename, myrepos);
                        setData=mcmcInterface.data.updateStruct(setData,setDataOverload);
                    else
                        error('unexpected xml node name encountered for file');
                    end
                elseif (strcmp(tNode.getNodeName,'element')==1)
                    numElements=numElements+1;
                    setData.elements(numElements,1)=mcmcInterface.element.readStructFromXMLNode(tNode);
                end
            end
        end

        function setData=updateStruct(setDataOriginal,setDataOverload)  
            stdout=1;
            fprintf(stdout, '\tOverloading fields in data\n');

            setData=setDataOriginal;

            overloadfieldnameslist=fieldnames(setDataOverload);
            if(any(contains(overloadfieldnameslist,'uuid')))
                setData.uuid=setDataOverload.uuid;
            end
            if(any(contains(overloadfieldnameslist,'description')))
                setData.description=setDataOverload.description;
            end    
            if(any(contains(overloadfieldnameslist,'author')))
                setData.author=setDataOverload.author;
            end
            if(any(contains(overloadfieldnameslist,'reference')))
                setData.reference=setDataOverload.reference;
            end              
            if(any(contains(overloadfieldnameslist,'elements')))
                setData.elements=setDataOverload.elements;
            end
            if(any(contains(overloadfieldnameslist,'dirData')))
                setData.dirData=setDataOverload.dirData;
            end
            if(any(contains(overloadfieldnameslist,'fileData')))
                setData.fileData=setDataOverload.fileData;
            end            
        end        

        function retData=loadAndConstructData(xmlfile)
            setData=mcmcInterface.data.readStructFromXMLFile(xmlfile);		            
            retData=mcmcInterface.data.constructor(setData);
        end

        function retData=constructor(setData)
            stdout=1;
    		fprintf(stdout, 'Constructing the data object\n');
            for i=1:size(setData.elements,1)
                setElements(i,1)=mcmcInterface.element.constructor(setData.elements(i));
            end
            retData=mcmcInterface.data(setData.uuid,setData.description,setData.author,setData.reference,setElements,setData.dirData,setData.fileData);
        end          

        function retver=getVersion()%this allows us to read the private constant
            retver=mcmcInterface.data.version;
        end        
    end    
    
    methods
        function obj = data(setUuid,setDescription,setAuthor,setReference,setElements,setDirData,setFileData)
            %data
            %   Construct an instance of this class
            if nargin>0
                obj.uuid=setUuid;
                obj.description=setDescription;
                obj.author=setAuthor;
                obj.reference=setReference;
                obj.elements=setElements;
                obj.dirData=setDirData;
                obj.fileData=setFileData;
            end
        end

        function quickLook(obj)
            stdout=1;
            fprintf(stdout,'\tData=%s/%s\n',obj.dirData,obj.fileData);
            numElements=size(obj.elements,1);
            for i=1:numElements
                fprintf(stdout,'\t\t');
                limitValues=true;
                obj.elements(i).quickLook(limitValues)
            end
        end        
    end

    methods %getter/setter functions
        function obj=set.elements(obj,setElements) 
            if ~isa(setElements,'mcmcInterface.element')
                error('elements is not mcmcInterface.element object');
            end
            obj.elements=setElements;
        end    
    end
end

