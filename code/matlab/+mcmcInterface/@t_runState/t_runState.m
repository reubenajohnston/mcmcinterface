classdef t_runState < int32
    enumeration
        Idle (1)
        Running (2)
        Finished (3)
        Error (4)
    end
    
    methods (Static)
        function value=fromstring(strvalue)
            if strcmp('mcmcInterface.t_runState.Idle',strvalue)==1
                value=mcmcInterface.t_runState.Idle;
            elseif strcmp('mcmcInterface.t_runState.Running',strvalue)==1
                value=mcmcInterface.t_runState.Running;
            elseif strcmp('mcmcInterface.t_runState.Finished',strvalue)==1
                value=mcmcInterface.t_runState.Finished;
            elseif strcmp('mcmcInterface.t_runState.Error',strvalue)==1
                value=mcmcInterface.t_runState.Error;
            else
                error('Unknown mcmcInterface.t_runState');
            end
        end
    end    

    methods
        function strvalue=tostring(obj)
            switch obj
                case mcmcInterface.t_runState.Idle
                    strvalue='mcmcInterface.t_runState.Idle';
                case mcmcInterface.t_runState.Running
                    strvalue='mcmcInterface.t_runState.Running';  
                case mcmcInterface.t_runState.Finished
                    strvalue='mcmcInterface.t_runState.Finished';  
                case mcmcInterface.t_runState.Error
                    strvalue='mcmcInterface.t_runState.Error';  
                otherwise
                    error('unsupported mcmcInterface.t_runState type');
            end
        end    
    end    
end

