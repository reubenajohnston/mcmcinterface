function S=coda2mat(file_ind,file_out)
%CODA2MAT  Read CODA output (OpenBUGS or JAGS) to matlab structure
%
% S=coda2mat(file_ind,file_out)
%  file_ind - index file (in ascii format) 
%  file_out - output file (in ascii format)
%  S        - matlab structure, with CODA variables as fields
%
% The samples are stored so that N=2 x M=3 variable R with Tau=1000 samples would be returned as S.R(2,3,1000)
%
% Note1: The data is returned in a structure that makes extraction of individual sample sequences easy: the sequences are directly NxMxTau matrices 
%        of type double, where Tau is the number of samples.
% - A scalar for variable R with Tau samples would be S.R(Tau,1,)
% - A Nx1 vector for variable R with Tau samples would be S.R(N,1,Tau)
% - A NxM matrix for variable R with Tau samples would be S.R(N,M,Tau)
%
% Note2: In variable names "." is replaced with "_"

% (c) Jouko.Lampinen@hut.fi, 2000
% 2003-01-14 Aki.Vehtari@hut.fi - Replace "." with "_" in variable names
% 2017 reuben@reubenjohnston.com - Reworked significantly

ind=readfile(file_ind);

timerStart=tic;

%method 3
%CODAchain file format is:
%sample1 value
%sample2 value
%...
%sampleN value
%CODAindex file format is:
%varName[dim1,dim2] startSample# stopSample#
data=dlmread(file_out);

elapsedtime=toc(timerStart);
elapsedseconds=seconds(elapsedtime);
rawelapsedseconds=seconds(elapsedseconds);
tempstr=sprintf('\tElapsed read time for %s was %d seconds.', file_out, rawelapsedseconds);
disp(tempstr);

Nvars=size(ind,1);
S=[];%clear the output struct S
for k=1:Nvars
    %only support scalars, vectors, 2-D matrices
    [varnamestr,indexstr]=strtok(ind(k,:));
    varnamestr=strrep(varnamestr,'.','_');%matlab does not support '.' in the variable names
    varnamestr=strrep(varnamestr,'[','(');%change '[' to '('
    varnamestr=strrep(varnamestr,']',')');%change ']' to ')'
    indices=str2num(indexstr);
    if size(indices)~=[1 2]
        error(['Cannot read line: [' ind(k,:) ']']);%format for content in CODAchain file has issues
    end
    samples=data(indices(1):indices(2),2);
    leftParenIdx=find(varnamestr=='(');
    rightParenIdx=find(varnamestr==')');
    if ~isempty(leftParenIdx)%it is a vector or a 2D-matrix we need to reshape it
        indices=str2num(sprintf('[%s]',varnamestr(leftParenIdx+1:rightParenIdx-1)));
        outStructStr=sprintf('%s(%d,:)',varnamestr(1:leftParenIdx-1),indices(1));
        evalStr=sprintf('S.%s=samples'';',outStructStr);
    else%it is a scalar
        outStructStr=sprintf('%s(:,1,1)',varnamestr);%should not have any parenthesis in the name
        evalStr=sprintf('S.%s=samples;',outStructStr);
    end
    eval(evalStr);
end

function T=readfile(filename)
f=fopen(filename,'r');
if f==-1, fclose(f); error(filename); end
i=1;
while 1
    clear line;
    line=fgetl(f);
    if ~isstr(line), break, end
    n=length(line);
    T(i,1:n)=line(1:n);
    i=i+1;
end
fclose(f);

