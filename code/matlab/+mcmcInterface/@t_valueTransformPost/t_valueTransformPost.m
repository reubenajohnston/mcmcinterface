classdef t_valueTransformPost < int32
    enumeration
        none (1)
        fromlog10 (2)%convert back from data scaled using log10(data)
        fromln (3)
        fromStandardized (4)%convert back from data standardized using (data-mean(data))/std(data)
        fromlog10Standardized (5)%convert back from data transformed by taking log10(t) of data that is standardized
        fromMeanCentered (6)
        adjustForFit2NormalizedData (7)
        adjustForFit2MeanCenteredData (8)
        %put externals in t_valueTransformPost_ext
    end

    methods (Static)                
        function enumobj=string2enum(strval)
            if strcmp(strval,'mcmcInterface.t_valueTransformPost.none')==1
                enumobj=mcmcInterface.t_valueTransformPost.none;
            elseif strcmp(strval,'mcmcInterface.t_valueTransformPost.fromlog10')==1
                enumobj=mcmcInterface.t_valueTransformPost.fromlog10;
            elseif strcmp(strval,'mcmcInterface.t_valueTransformPost.fromln')==1
                enumobj=mcmcInterface.t_valueTransformPost.fromln;
            elseif strcmp(strval,'mcmcInterface.t_valueTransformPost.fromStandardized')==1
                enumobj=mcmcInterface.t_valueTransformPost.fromStandardized;
            elseif strcmp(strval,'mcmcInterface.t_valueTransformPost.fromlog10Standardized')==1
                enumobj=mcmcInterface.t_valueTransformPost.fromlog10Standardized;
            elseif strcmp(strval,'mcmcInterface.t_valueTransformPost.fromMeanCentered')==1
                enumobj=mcmcInterface.t_valueTransformPost.fromMeanCentered;
            elseif strcmp(strval,'mcmcInterface.t_valueTransformPost.adjustForFit2NormalizedData')==1
                enumobj=mcmcInterface.t_valueTransformPost.adjustForFit2NormalizedData;
            elseif strcmp(strval,'mcmcInterface.t_valueTransformPost.adjustForFit2MeanCenteredData')==1
                enumobj=mcmcInterface.t_valueTransformPost.adjustForFit2MeanCenteredData;
            else
                error('todo: external type lookup')
                %enumobj=mcmcInterface.t_valueTransformPost_ext.string2enum(strval);
            end
        end
        
        function strvalue=tostring(enumobj)
            switch enumobj
                case mcmcInterface.t_valueTransformPost.none
                    strvalue='mcmcInterface.t_valueTransformPost.none';
                case mcmcInterface.t_valueTransformPost.fromlog10
                    strvalue='mcmcInterface.t_valueTransformPost.fromlog10';
                case mcmcInterface.t_valueTransformPost.fromln
                    strvalue='mcmcInterface.t_valueTransformPost.fromln';
                case mcmcInterface.t_valueTransformPost.fromStandardized
                    strvalue='mcmcInterface.t_valueTransformPost.fromStandardized';
                case mcmcInterface.t_valueTransformPost.fromlog10Standardized
                    strvalue='mcmcInterface.t_valueTransformPost.fromlog10Standardized';
                case mcmcInterface.t_valueTransformPost.fromMeanCentered
                    strvalue='mcmcInterface.t_valueTransformPost.fromMeanCentered';
                case mcmcInterface.t_valueTransformPost.adjustForFit2NormalizedData
                    strvalue='mcmcInterface.t_valueTransformPost.adjustForFit2NormalizedData';
                case mcmcInterface.t_valueTransformPost.adjustForFit2MeanCenteredData
                    strvalue='mcmcInterface.t_valueTransformPost.adjustForFit2MeanCenteredData';
                otherwise
                    error('todo: external type lookup')
                    %strvalue=mcmcInterface.t_valueTransformPost_ext.tostring(enumobj);
            end
        end
    end
end