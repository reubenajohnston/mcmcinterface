classdef distribution
    %DISTRIBUTION 
    %   This class, mcmcInterface.distribution, is used as a data structure for storing information describing the stochastic nature of elements.
    
    properties
        type                % distribution type (mcmcInterface.t_elementDistribution)
        expression          % stochastic expression for the type that uses the sampler's language (e.g., a simulation using Stan would have distributions expressed using language of the Stan sampler)
    end

    methods (Static)
        function setDistribution = readStructFromXMLNode(distributionNode)
            %stdout=1;
            %fprintf(stdout, '\tLoading the distribution from distributionNode\n');

            setDistribution.type=strcat('mcmcInterface.t_elementDistribution.',cast(distributionNode.getAttribute('type'),'char'));

            setDistribution.expression=''; % default is empty (e.g., for fixed)

            childNodeList=distributionNode.getChildNodes();
            for i=1:childNodeList.getLength()
                tNode=childNodeList.item(i-1);
                if (strcmp("expression",tNode.getNodeName())==1)
                    setDistribution.expression=cast(tNode.getTextContent(),'char');
                end
            end
        end

        function retDistribution=constructor(setDistribution)
            %stdout=1;
    		%fprintf(stdout, 'Constructing the distribution object\n');
            retDistribution=mcmcInterface.distribution(setDistribution.type,setDistribution.expression);
        end        
    end

    methods
        function obj = distribution(setType,setExpression)
            %STATISTICS Construct an instance of this class
            obj.type=mcmcInterface.t_elementDistribution.str2enum(setType);
            obj.expression=setExpression;
        end
        
        function retNode = createXmlNodeFromObject(obj,docNode)
        %         <distribution>
        %             <type></type>
        %             <expression></expression>
        %         </distribution>
            retNode=docNode.createElement('distribution');

            tNode=docNode.createElement('type');
            tNode.appendChild(docNode.createTextNode(obj.type.tostring));
            retNode.appendChild(tNode);     

            tNode=docNode.createElement('expression');
            tNode.appendChild(docNode.createTextNode(obj.expression));
            retNode.appendChild(tNode);                    
        end
    end

    methods %getter/setter functions
        function obj=set.type(obj, setType)
            if ~(isa(setType,'mcmcInterface.t_elementDistribution'))
                error('setType is not mcmcInterface.t_elementDistribution object');
            end
            obj.type=setType;
        end
    end       
end

