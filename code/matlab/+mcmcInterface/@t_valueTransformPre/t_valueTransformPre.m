classdef t_valueTransformPre < int32
    enumeration
        none (1)
        tolog10 (2)%scaled using log10(data)
        toln (3)
        toStandardized (4)%data standardized using (data-mean(data))/std(data)
        tolog10Standardized (5)%log10(t) data that is standardized
        toMeanCentered (6)
        %put externals in t_valueTransformPre_ext
    end
    methods (Static)
        function enumobj=string2enum(strval)
            if strcmp(strval,'mcmcInterface.t_valueTransformPre.none')==1
                enumobj=mcmcInterface.t_valueTransformPre.none;
            elseif strcmp(strval,'mcmcInterface.t_valueTransformPre.tolog10')==1
                enumobj=mcmcInterface.t_valueTransformPre.tolog10;
            elseif strcmp(strval,'mcmcInterface.t_valueTransformPre.toln')==1
                enumobj=mcmcInterface.t_valueTransformPre.toln;
            elseif strcmp(strval,'mcmcInterface.t_valueTransformPre.toStandardized')==1
                enumobj=mcmcInterface.t_valueTransformPre.toStandardized;
            elseif strcmp(strval,'mcmcInterface.t_valueTransformPre.tolog10Standardized')==1
                enumobj=mcmcInterface.t_valueTransformPre.tolog10Standardized;
            elseif strcmp(strval,'mcmcInterface.t_valueTransformPre.toMeanCentered')==1
                enumobj=mcmcInterface.t_valueTransformPre.toMeanCentered;
            else 
                error('todo: external type lookup')
                %enumobj=mcmcInterface.t_valueTransformPre_ext.string2enum(strval);
            end
        end
        
        function strvalue=tostring(enumobj)
            switch enumobj
                case mcmcInterface.t_valueTransformPre.none
                    strvalue='mcmcInterface.t_valueTransformPre.none';
                case mcmcInterface.t_valueTransformPre.tolog10
                    strvalue='mcmcInterface.t_valueTransformPre.tolog10';
                case mcmcInterface.t_valueTransformPre.toln
                    strvalue='mcmcInterface.t_valueTransformPre.toln';
                case mcmcInterface.t_valueTransformPre.toStandardized
                    strvalue='mcmcInterface.t_valueTransformPre.toStandardized';
                case mcmcInterface.t_valueTransformPre.tolog10Standardized
                    strvalue='mcmcInterface.t_valueTransformPre.tolog10Standardized';
                case mcmcInterface.t_valueTransformPre.toMeanCentered
                    strvalue='mcmcInterface.t_valueTransformPre.toMeanCentered';
                otherwise
                    error('todo: external type lookup')
                    %strvalue=mcmcInterface.t_valueTransformPre_ext.tostring(enumobj);
            end
        end
    end
end