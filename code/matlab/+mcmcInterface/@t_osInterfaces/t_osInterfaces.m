classdef t_osInterfaces < int32
    enumeration
        WindowsOS (1)
        LinuxOS (2)
        MacOS (3)
    end

    methods (Static)       
        function enumobj=str2enum(strval)
            if strcmp('mcmcInterface.t_osInterfaces.WindowsOS',strval)==1
                enumobj=mcmcInterface.t_osInterfaces.WindowsOS;
            elseif strcmp('mcmcInterface.t_osInterfaces.LinuxOS',strval)==1
                enumobj=mcmcInterface.t_osInterfaces.LinuxOS;
            elseif strcmp('mcmcInterface.t_osInterfaces.MacOS',strval)==1
                enumobj=mcmcInterface.t_osInterfaces.MacOS;
            else
                error('unsupported mcmcInterface.t_osInterfaces type');
            end
        end        
    end

    methods
        function strvalue=tostring(obj)
            switch obj
                case mcmcInterface.t_osInterfaces.WindowsOS
                    strvalue='mcmcInterface.t_osInterfaces.WindowsOS';
                case mcmcInterface.t_osInterfaces.LinuxOS
                    strvalue='mcmcInterface.t_osInterfaces.LinuxOS';  
                case mcmcInterface.t_osInterfaces.MacOS
                    strvalue='mcmcInterface.t_osInterfaces.MacOS';  
                otherwise
                    error('unsupported mcmcInterface.t_osInterfaces type');
            end
        end    
    end    
end

