classdef t_elementDistribution < int32
    enumeration
        fixed (1)
        flat (2)
        standard (3)
        custom (4)        %e.g., regression equation, stochastic process (like the NHPP), distribution in a hierarchical system, etc.
        transformed (5)   %e.g., variables that are transformed or computed from other variables being sampled
        generated (6)     %e.g., randomly generated numbers that use variables as input to the rng distribution
        sampled (7)       %e.g., sampled from posterior distribution
        %do we need o be able to extend t_elementDistribution and distribution?
    end
    
    methods (Static)        
        function enumobj=str2enum(strval)
            if strcmp('mcmcInterface.t_elementDistribution.fixed',strval)==1
                enumobj=mcmcInterface.t_elementDistribution.fixed;
            elseif strcmp('mcmcInterface.t_elementDistribution.flat',strval)==1
                enumobj=mcmcInterface.t_elementDistribution.flat;
            elseif strcmp('mcmcInterface.t_elementDistribution.standard',strval)==1
                enumobj=mcmcInterface.t_elementDistribution.standard;
            elseif strcmp('mcmcInterface.t_elementDistribution.custom',strval)==1
                enumobj=mcmcInterface.t_elementDistribution.custom;
            elseif strcmp('mcmcInterface.t_elementDistribution.transformed',strval)==1
                enumobj=mcmcInterface.t_elementDistribution.transformed;
            elseif strcmp('mcmcInterface.t_elementDistribution.generated',strval)==1
                enumobj=mcmcInterface.t_elementDistribution.generated;
            elseif strcmp('mcmcInterface.t_elementDistribution.sampled',strval)==1
                enumobj=mcmcInterface.t_elementDistribution.sampled;
            else
                error('unsupported mcmcInterface.t_elementDistribution type');
            end
        end
    end

    methods        
        function strvalue=tostring(obj)
            switch obj
                case mcmcInterface.t_elementDistribution.fixed
                    strvalue='mcmcInterface.t_elementDistribution.fixed';
                case mcmcInterface.t_elementDistribution.flat
                    strvalue='mcmcInterface.t_elementDistribution.flat';
                case mcmcInterface.t_elementDistribution.standard
                    strvalue='mcmcInterface.t_elementDistribution.standard';
                case mcmcInterface.t_elementDistribution.custom
                    strvalue='mcmcInterface.t_elementDistribution.custom';
                case mcmcInterface.t_elementDistribution.transformed
                    strvalue='mcmcInterface.t_elementDistribution.transformed';                    
                case mcmcInterface.t_elementDistribution.generated
                    strvalue='mcmcInterface.t_elementDistribution.generated';   
                case mcmcInterface.t_elementDistribution.sampled
                    strvalue='mcmcInterface.t_elementDistribution.sampled';  
                otherwise
                    error('unsupported mcmcInterface.t_elementDistribution type');
            end
        end
    end
end

