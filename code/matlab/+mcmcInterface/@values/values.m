classdef values
    %VALUES 
    %   Class that represents values in data, variables, and samples files.
    
    properties
        hValueAttributes %handle to the corresponding valueAttributes object
        valueConstant    %value object of type t_valueDataType.constant with initial values (used for variables that need initial values), input data, or values for constant parameters
        valueGenerated   %value object of type t_valueDataType.generated with values sampled, generated, or predicted
    end

    methods (Static)
        function setValues = readStructFromXMLNode(valueNode)
            %stdout=1;
            %fprintf(stdout, '\tLoading the values from valueNode\n');

            tempStr=strcat('mcmcInterface.t_valuesDimensions.',cast(valueNode.getAttribute('dimensions'),'char'));
            setValues.dimensions=mcmcInterface.t_valuesDimensions.str2enum(tempStr);
            if isempty(setValues.dimensions)
                error('dimensions attribute missing from values node');
            end

            setValues.type=cast(valueNode.getAttribute('type'),'char');
            if isempty(setValues.type)
                setValues.type='double';%default if missing
            end
            temp=cast(valueNode.getAttribute('lowerBound'),'char');
            if isempty(temp)
                setValues.lowerBound=-Inf;%-Inf if lowerBound missing
            else
                setValues.lowerBound=str2num(temp);
            end
            temp=cast(valueNode.getAttribute('upperBound'),'char');
            if isempty(temp)
                setValues.upperBound=Inf;%Inf if upperBound missing
            else
                setValues.upperBound=str2num(temp);
            end
            temp=cast(valueNode.getAttribute('precision'),'char');
            if isempty(temp)
                setValues.precision=1.0e-3;%1e-3 if precision missing
            else
                setValues.precision=str2num(temp);
            end

            setValues.valConstant=struct([]);%empty struct if missing
            setValues.valGenerated=struct([]);%empty struct if missing

            %get valConstant and valGenerated if either or both is present
            childNodeList=valueNode.getChildNodes();
            for i=1:childNodeList.getLength()
                tNode=childNodeList.item(i-1);
                if (strcmp("value",tNode.getNodeName())==1)
                    tmpValue=mcmcInterface.value.readStructFromXMLNode(tNode);
                    if tmpValue.type==mcmcInterface.t_valueDataType.constant
                        setValues.valConstant=tmpValue;
                    else
                        setValues.valGenerated=tmpValue;
                    end
                end
            end
        end

        function retValues = constructor(setValues)
            %stdout=1;
    		%fprintf(stdout, 'Constructing the values object\n');
            setValueAttributes = mcmcInterface.valueAttributes(setValues.dimensions,setValues.type,setValues.lowerBound,setValues.upperBound,setValues.precision);
            if ~isempty(setValues.valConstant)
                setData=mcmcInterface.value.fromString(setValues.valConstant.data,setValueAttributes.dimensions,setValueAttributes.type);
                setValueConstant=mcmcInterface.value(setValueAttributes,setValues.valConstant.type, setValues.valConstant.tfPre, setValues.valConstant.tfPost, setData);
            else
                setValueConstant=struct([]);%empty struct
            end
            if ~isempty(setValues.valGenerated)
                setData=mcmcInterface.value.fromString(setValues.valGenerated.data,setValueAttributes.dimensions,setValueAttributes.type);
                setValueGenerated=mcmcInterface.value(setValueAttributes,setValues.valGenerated.type, setValues.valGenerated.tfPre, setValues.valGenerated.tfPost, setData);
            else
                setValueGenerated=struct([]);%empty struct
            end
            retValues=mcmcInterface.values(setValueAttributes,setValueConstant,setValueGenerated);
        end 
    end

    methods
        function obj = values(setValueAttributes,setValueConstant,setValueGenerated)
            %VALUES Construct an instance of this class
            %   Detailed explanation goes here
            obj.hValueAttributes = setValueAttributes;
            if isempty(setValueConstant)
                obj.valueConstant=mcmcInterface.value.empty;
            else
                obj.valueConstant=setValueConstant;
            end
            if isempty(setValueGenerated)
                obj.valueGenerated=mcmcInterface.value.empty;
            else
                obj.valueGenerated=setValueGenerated;
            end            
        end

        function retNode = createXmlNodeFromObject(obj,docNode)
        % 	      <values dimensions="scalar" 
        % 			    type="double"
        % 			    lowerBound=""
        % 			    upperBound=""
        % 			    precision="1.0E-1">
        % 		        <constant>[0]</constant><!--initial value-->
        %               <generated>[]</generated><!--samples-->
        % 	      </values>
            retNode=docNode.createElement('values');
            retNode.setAttribute('dimensions',obj.dimensions.tostring());
            retNode.setAttribute('type',obj.type);
            retNode.setAttribute('lowerBound',num2str(obj.lowerBound));
            retNode.setAttribute('upperBound',num2str(obj.upperBound));
            retNode.setAttribute('precision',num2str(obj.precision));

            tNode=docNode.createElement('constant');
            tNode.appendChild(docNode.createTextNode(obj.constant2String()));
            retNode.appendChild(tNode);            

            tNode=docNode.createElement('generated');
            tNode.appendChild(docNode.createTextNode(obj.generated2String));
            retNode.appendChild(tNode);             
        end
    end
end

