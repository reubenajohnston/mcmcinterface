classdef storageObjectPath
    %STORAGEOBJECTPATH XML node parser helper for directory (<dir>) and file (<file>) elements
    %   Helper class for directory that passes in a <dir> or <file> node above and stores a tuple with the node's 
    % name/absolute path in this class.  
    %   * Use nameonly for cases where there is no relative or absolute path to include with the storage object.  
    %   * Use absolute to specify the included value as the absolute path to the storage object.
    %   * Use relative to determine the path's preceding contents from .mrconfig and repository and append it 
    %     with the provided value
    
    %Instances to parse:
	%<dir name="dirSimulation" type="absolute">/sandboxes/<USER>/mcmcbayes/models/exponential/config</dir>
	%<dir name="dirSimulation" type="relative" repository="exponential.git">config</dir>

    properties
        name
        path
    end
    
    methods
        function obj = storageObjectPath(rootNode, myrepos)
            %STORAGEOBJECTPATH Construct an instance of this class
            % rootNode-data structure with the xml node read in via xmlread
            % myrepos (optional)-myrepos object with base dir and list of repositories

            obj.name=cast(rootNode.getAttribute('name'),'char');

            if (exist('myrepos','var'))
                strType=cast(rootNode.getAttribute('type'),'char');
                if (strcmp(strType,'absolute')==1)
                    %if absolute, store the path from the element
                    obj.path=cast(rootNode.getTextContent(),'char');
                elseif (strcmp(strType,'relative')==1)
                    %if relative, get the base directory from myrepos, then append the repository path and element contents to it
                    %repository attribute is needed only for relative type
                    strRepository=cast(rootNode.getAttribute('repository'),'char');
                    strRepositoryPath=myrepos.getRepoLocation(strRepository);
                    strRelativePath=cast(rootNode.getTextContent(),'char');
                    obj.path=strcat(myrepos.baseDirectory,'/',strRepositoryPath,'/',strRelativePath);
                elseif (strcmp(strType,'nameonly')==1)
                    %there shouldn't be any path in the text content, only the dir name
                    obj.path=cast(rootNode.getTextContent(),'char');
                else
                    error('xml does not have type=relative or absolute for directory');
                end
            else
                %absolute
                obj.path=cast(rootNode.getTextContent(),'char');
            end
        end
    end
end

