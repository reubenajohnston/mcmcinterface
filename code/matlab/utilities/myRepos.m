classdef myRepos
    %MYREPOS Utility class to extract (from .mrconfig) and store (in an instance of this class):
    % 1) absolute path to base directory from mrconfigfile
    % 2) list of repositories (sections) in mrconfigfile
    properties (Constant)
        dirTemp='/tmp' %only for posix
        python='python3'
    end
    
    properties
        baseDirectory
        locations
        repositories
    end

    methods
        function obj = myRepos(mrconfigfile, picklemyrepospython)
            %mrconfigfile and myrepospython (path to pickleMyReposSections.py) have to be passed in 
            % so as not to disclose paths in code or config files committed that might have usernames 
            % or other sensitive info)
            stdout=1;

            %python3 pickleMyReposSections.py --myrepos mrconfigfile --outputdir dirTemp
            argsstr=sprintf('--myrepos %s --outputdir %s', mrconfigfile, obj.dirTemp);
            commandstr=sprintf('%s %s %s', obj.python, picklemyrepospython, argsstr);

            %check that myrepospython exists
            if ~exist(picklemyrepospython,'file')
                error('Must provide a valide path to pickleMyReposSections.py');
            end
            %fprintf(stdout, '\tdebug: myRepos is calling:  %s\n', commandstr);
            [status, cmdout]=system(commandstr);
            %fprintf(stdout, '\tdebug: myRepos system() returned status=%d, cmdout=%s\n', status, cmdout);
 
            pyenv(Version=obj.python);
            tempfilename=strcat(obj.dirTemp, '/', 'dirname.pkl');
            fid = py.open(tempfilename, 'rb');
            DIRNAME = py.pickle.load(fid);
            tempfilename=strcat(obj.dirTemp, '/', 'sections.pkl');
            fid = py.open(tempfilename, 'rb');
            pysections = py.pickle.load(fid);
            tempfilename=strcat(obj.dirTemp, '/', 'repositories.pkl');
            fid = py.open(tempfilename, 'rb');
            pyrepositories = py.pickle.load(fid);

            obj.baseDirectory = cast(DIRNAME{1},'char');

            obj.locations = {};
            for pysection = pysections{1}
                obj.locations{end+1,1}=cast(pysection{1},'char');
            end

            obj.repositories = {};
            for pyrepository = pyrepositories{1}
                obj.repositories{end+1,1}=cast(pyrepository{1},'char');
            end            
        end

        function location=getRepoLocation(obj, repository)
            %This function looks up the subfolder that the repository is cloned into

            index=-1;
            for i=1:numel(obj.repositories)
                %find index for repository in obj.repositories
                if strcmp(obj.repositories(i),repository)==1
                    index=i;
                end
            end

            if index==-1
                error('Did not locate repository in myrepos');
            end

            location=obj.locations{index};
        end
    end
end