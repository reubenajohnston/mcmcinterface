%filename-file to replace the buffer 
%magicstrstart-string to locate start of the buffer to replace (starting on following line)
%magicstrstop-string to locate end of the buffer to replace (stopping on previous line)
%buffer-string to insert
function freplacebuffer(filename, magicstrstart, magicstrstop, cellbuffer)
file=java.io.File(filename);
if ~file.exists()%simpler and more reliable existence check for a file (vs MATLAB's exist)
    error('File does not exist');
end
fileID = fopen(filename,'r');
assert(fileID>=0, 'Problem opening the file');

numline=numel(cellbuffer);

eof=0;
while (eof==0) %read file into a cell array line by line
    tstring=fgets(fileID);%read line and keep newlines
    if exist('tstring','var')==1
        tcell=cellstr(tstring);
        if exist('filebuffer')==0
            filebuffer=tcell;
        else
            filebuffer=[filebuffer tcell];
        end
    end
    eof=feof(fileID);
end
fclose(fileID);
%find the index of magicstr start
found=false;
len=numel(magicstrstart);
index=1;
while (found==false)
    stringout=filebuffer{index};%read the next line
    if numel(stringout)>len
        stringout=stringout(1:len);%compare len(magicstr) characters
    end
    if (strcmp(stringout,magicstrstart)==1)
        found=true;
        startindex=index+1;
        break;
    end
    index=index+1;
    assert(index<=numel(filebuffer), 'Problem locating magicstrstart');
end
%find the index of magicstr stop
found=false;
len=numel(magicstrstop);
while (found==false)
    stringout=filebuffer{index};%read the next line
    if numel(stringout)>len
        stringout=stringout(1:len);%compare len(magicstr) characters
    end
    if (strcmp(stringout,magicstrstop)==1)
        found=true;
        stopindex=index+1;
        break;
    end
    index=index+1;
    assert(index<=numel(filebuffer), 'Problem locating magicstrstop');
end
%copy filebuffer(1:startindex-1) into tempbuffer
tempbuffer=filebuffer(1:startindex-1);
%insert buffer at end of tempbuffer
tempbuffer=[tempbuffer cellbuffer];
%copy stopindex-1:numel(filebuffer) to end of tempbuffer
tempbuffer=[tempbuffer filebuffer(stopindex-1:numel(filebuffer))];
fileID = fopen(filename,'w');%'w', deletes the file contents
assert(fileID>=0, 'Problem opening the file');
%write cell array tempbuffer into file line by line
for index=1:numel(tempbuffer)
    fprintf(fileID,'%s\n',char(tempbuffer{index}));  
end
fclose(fileID);