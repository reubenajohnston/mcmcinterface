classdef logging 
    methods (Static)
        function startLogging(filename)
            status=get(0,'Diary');%check if it is enabled
            %clc
            if strcmp(status,'on')
                diary off;%turn it off
            end         
            diary(filename)%enable it
        end
        
        function stopLogging()
            status=get(0,'Diary');%check if it is enabled
            if strcmp(status,'off')                
                return;%Logging was already off
            end            
            diary off;%turn it off
        end
        
        function stopAndSaveLog(tempLog,permLog)
            status=get(0,'Diary');%check if it is enabled
            if strcmp(status,'off')
                warning('Logging was already off, not saving.');
                return;
            end
            diary off;%turn it off
            [status,message,messageId] = movefile(tempLog,permLog);
        end        
    end
end
