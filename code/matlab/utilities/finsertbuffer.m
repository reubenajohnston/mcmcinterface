%filename-file to insert the buffer 
%magicstr-string to locate and insert the buffer on the following line
%buffer-string to insert
function finsertbuffer(filename, magicstr, buffer)
fileID = fopen(filename,'r');
assert(fileID>=0, 'Problem opening the file');

cellbuffer = strread(buffer, '%s', 'delimiter', sprintf('\n'));
numline=numel(cellbuffer);

eof=0;
while (eof==0) %read file into a cell array line by line
    tstring=fgets(fileID);%read line and keep newlines
    if exist('tstring','var')==1
        tcell=cellstr(tstring);
        if exist('filebuffer','var')==0
            filebuffer=tcell;
        else
            filebuffer=[filebuffer tcell];
        end
    end
    eof=feof(fileID);
end
fclose(fileID);
%find the index of magicstr
found=false;
len=numel(magicstr);
index=1;
while (found==false)
    stringout=filebuffer{index};%read the next line
    if numel(stringout)>len
        stringout=stringout(1:len);%compare len(magicstr) characters
    end
    if (strcmp(stringout,magicstr)==1)
        found=true;
        break;
    end
    index=index+1;
    assert(index<=numel(filebuffer), 'Problem locating magicstr');
end
%copy filebuffer(1:index) into tempbuffer
tempbuffer=filebuffer(1:index);
%insert buffer at end of tempbuffer
tempbuffer=[tempbuffer cellbuffer'];
%copy position+1+numel(buffer) to end of tempbuffer
tempbuffer=[tempbuffer filebuffer(index+1+numline:numel(filebuffer))];
fileID = fopen(filename,'w');%'w', deletes the file contents
assert(fileID>=0, 'Problem opening the file');
%write cell array tempbuffer into file line by line
for index=1:numel(tempbuffer)
    fprintf(fileID,'%s\n',char(tempbuffer{index}));  
end
fclose(fileID);