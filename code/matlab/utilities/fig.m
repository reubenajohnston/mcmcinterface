function [hfig]=fig()%plot figures only when Desktop is not minimized
    %hfig is type matlab.ui.Figure
    dtframe = com.mathworks.mlservices.MatlabDesktopServices.getDesktop.getMainFrame;
    if isempty(dtframe)
        setVisibility('Off');%hide  all figures
        set(groot, 'DefaultFigureVisible', 'Off');%default always off, needs to be first
    else
        if (get(dtframe,'Minimized')==1) || ~(usejava('Desktop'))%needs to happen before the figure manipulation below
            setVisibility('Off');%hide  all figures
            set(groot, 'DefaultFigureVisible', 'Off');%default always off, needs to be first
        else
            setVisibility('On');%show all figures
            set(groot, 'DefaultFigureVisible', 'On');%default always off, needs to be first
        end
    end
    
    f=figure();%create the figure
    set(groot,'CurrentFigure',f.Number)%make the new figure the current graphics object
%debugging start
%     tf=gcf;
%     fighandles=get(groot,'Children');
%     tempstr=sprintf('Created new figure %d, total figures are now %d, current figure is %d',f.Number,numel(fighandles),tf.Number);    
%     disp(tempstr);
%debugging end
    hfig=f;
end