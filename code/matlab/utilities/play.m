function play(filename)
    [y,Fs]=audioread(filename);
    player=audioplayer(y,Fs);
    player.play();
    while (player.isplaying==true)
        pause(0.1);
    end
end