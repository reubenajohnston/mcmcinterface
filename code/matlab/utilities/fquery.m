%filename-file to query
%magicstr-string to locate for reading following line
function [fieldbuffer]=fquery(filename, magicstr)
file=java.io.File(filename);
if ~file.exists()%simpler and more reliable existence check for a file (vs MATLAB's exist)
    fieldbuffer='NOTFOUND';
    return;
end
fileID = fopen(filename,'r');
assert(fileID>=0, 'Problem opening the file');

eof=0;
while (eof==0) %read file into a cell array line by line
    tstring=fgets(fileID);%read line and keep newlines
    if tstring~=-1
        tcell=cellstr(tstring);
        if exist('filebuffer','var')==0
            filebuffer=tcell;
        else
            filebuffer=[filebuffer tcell];
        end
    else
        fieldbuffer='NOTFOUND';
        fclose(fileID);
        return;
    end
    eof=feof(fileID);
end
fclose(fileID);
%find the index of magicstr
found=false;
len=numel(magicstr);
index=1;
while (found==false)
    stringout=filebuffer{index};%read the next line
    if numel(stringout)>len
        stringout=stringout(1:len);%compare len(magicstr) characters
    end
    if (strcmp(stringout,magicstr)==1)
        found=true;
        break;
    end
    index=index+1;
    if index>numel(filebuffer)
        fieldbuffer='NOTFOUND';
        return;
    end
end
%read the next line into fieldbuffer
fieldbuffer=cell2mat(filebuffer(index+1));
