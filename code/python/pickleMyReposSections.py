import argparse
import configparser
import errno
import os
import pickle
import xml.etree.ElementTree as ET

#Usage:
#$ python3 pickleMyReposSections.py --myrepos <ABSPATHTOMRCONFIG> --outputdir <ABSPATHTOOUTPUTFILE>
#$ python3 pickleMyReposSections.py --myrepos /sandboxes/<USER>/<STUDY>/.mrconfig --outputdir /tmp

#MATLAB:
#>> pyenv(Version="/home/johnsra2/venv/mcmcbayes/bin/python3")
#>> fid = py.open('/tmp/dirname.pkl','rb');
#>> dirname = py.pickle.load(fid)
#>> fid = py.open('/tmp/sections.pkl','rb');
#>> sections = py.pickle.load(fid)
#>> fid = py.open('/tmp/repositories.pkl','rb');
#>> repositories = py.pickle.load(fid)

# Each .mrconfig section needs to contain lines in the format below and there can't be any 
# repository names that appear more than once.
#
# [<SECTION>]
# repository = echo <REPOSITORYNAME>.git
# checkout = git clone <REPOSITORY_URL> <SUBFOLDER_FOR_CLONE>
# archive = git archive -o <REPOSITORYNAME>.zip HEAD
# pull = git pull
#
# .mrconfig example:
#
# [mcmcbayes/mcmcinterface]
# repository = echo mcmcinterface.git
# checkout = git clone 'https://gitlab.com/mcmcbayes/mcmcinterface.git' 'mcmcinterface'
# archive = git archive -o mcmcinterface.zip HEAD
# pull = git pull

def usage(parser):                                                            
    parser.print_help()
    exit()

def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('--myrepos', help='Absolute path to .mrconfig myrepos file')
    parser.add_argument('--outputdir', help='Absolute path to output directory for pickle files')

    args = parser.parse_args()

    config=configparser.ConfigParser()

    if (args.myrepos is not None) and (args.outputdir is not None):
        #get the base directory for the .mrconfig file
        DIRNAME=os.path.dirname(args.myrepos)

        #read in the myrepos .mrconfig file
        config.read(args.myrepos)
        sections=[]
        repositories=[]
        for section in config.sections(): #iterate over the sections (each is a repository)
            sections.append(section)
            temp=config[section]['repository'].split('echo ',1)[1]#split the string on 'echo ' and take only the contents after the split
            repositories.append(temp)

        with open(args.outputdir+'/sections.pkl', 'wb') as f:  # open a text file
            pickle.dump([sections], f) # serialize the list

        with open(args.outputdir+'/repositories.pkl', 'wb') as f:  # open a text file
            pickle.dump([repositories], f) # serialize the list        

        with open(args.outputdir+'/dirname.pkl', 'wb') as f:  # open a text file
            pickle.dump([DIRNAME], f) # serialize the list            
    else:
        usage()

if __name__ == "__main__":
    main()