# Contact:
#   Reuben Johnston <reuben.johnston@jhuapl.edu>
# Date:
#   2024-06-16
# Description:
#   genstartup.py is a simple script that parses a mcmcBayes .mrconfig myrepos file and then iterates 
#   through all its listed repositories, looking for relativePaths.xml in each.  composes a list for
#   MATLAB path and Java paths listed in the relativePaths.xml files and then generates a startup.m with
#   entries for them all.
# Example usage:
#   # python3 genstartup.py --myrepos <PATH_TO_MRCONFIG> --output <PATH_TO_STARTUP_M>
# Requirements:
#   1) Run on Redhat, Ubuntu, ...
#   2) configparser
# Links:
#   https://docs.python.org/3/library/configparser.html
#   https://docs.python.org/3/library/xml.etree.elementtree.html

import argparse
import configparser
import errno
import os
import xml.etree.ElementTree as ET

def usage(parser):                                                            
    parser.print_help()
    exit()

def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('--myrepos', help='Absolute path to .mrconfig myrepos file')
    parser.add_argument('--outfile', help='Absolute path for startup.m file to create')

    args = parser.parse_args()

    config=configparser.ConfigParser()

    if (args.myrepos is not None) and (args.outfile is not None):
        #get the base directory for the .mrconfig file
        DIRNAME=os.path.dirname(args.myrepos)

        #read in the myrepos .mrconfig file
        config.read(args.myrepos)
        matlabPaths=[]
        javaPaths=[]
        for section in config.sections(): #iterate over the sections (each is a repository)
            tempfile=os.path.join(DIRNAME,section,'relativePaths.xml')
            if os.path.isfile(tempfile):#if file exists, then read entries in it (PATH, JAVAPATH)
                #parse the *.xml 
                tree=ET.parse(tempfile)
                root=tree.getroot()#<relativePaths>
                for child in root:
                    childname=child.tag
                    if childname=='matlab': # parse <matlab> child node for <path> entries and append them to the MATLAB path list
                        for grandchild in child:
                            grandchildname=grandchild.tag
                            if grandchildname=='path':
                                if grandchild.text is None:
                                    tempdir=os.path.join(DIRNAME,section)
                                    tempdir=tempdir[0:len(tempdir)]#trim last '/'
                                else:
                                    tempdir=os.path.join(DIRNAME,section,grandchild.text)
                                
                                print(tempdir)

                                matlabPaths.append(tempdir)

                    #elif childname=='java': # parse <java> child node for <path> entries and append them to the Java path list
                    #else:

    #create the startup.m in the outfile 
    with open(args.outfile, "w") as f:
        curline='format long\n\n'
        f.write(curline)
        #for all the MATLAB path entries, create a line with `addpath(<PATHENTRY>)`
        for entry in matlabPaths:
            curline='addpath(\''+entry+'\')\n'
            f.write(curline)
        #for all the Java path entries, create a line with `javaaddpath(<PATHENTRY>)`
           
    print('Startup file creation was successful')

if __name__ == "__main__":
    main()
